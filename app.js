import * as ELEMENTS from './elements.js';
import {Http} from './http.js';
//import {Validator} from './validator.js';
import {WeatherData, WEATHER_PROXY_HANDLER} from './weather-data.js';

ELEMENTS.ELEMENT_SEARCH_BUTTON.addEventListener('click', searchWeather);
const APP_ID = '8e9b648df7ffadd06a00a934f14f676a';

function searchWeather() {
    // let METRIC = ELEMENTS.ELEMENTS_RADIO_METRIC.value;
    let METRIC = document.querySelector('input[name="metric"]:checked').value;

    const CITY_NAME = ELEMENTS.ELEMENT_SEARCHED_CITY.value.trim();

    if (CITY_NAME.length === 0 || validateString(ELEMENTS.ELEMENT_SEARCHED_CITY.value)){
        return alert('Please enter a valid city name');
    }

    ELEMENTS.ELEMENTS_LOADING_TEXT.style.display = 'block';
    ELEMENTS.ELEMENTS_WEATHER_BOX.style.display = 'none';

    const  URL = 'http://api.openweathermap.org/data/2.5/weather?q=' + CITY_NAME + '&units=imperial&appid=' + APP_ID;

    Http.fetchData(URL)
        .then(responseData => {
            const WEATHER_DATA = new WeatherData(CITY_NAME, responseData.weather[0].description.toUpperCase(), METRIC);
            const  WEATHER_PROXY = new Proxy(WEATHER_DATA, WEATHER_PROXY_HANDLER);
            WEATHER_PROXY.temperature = responseData.main.temp;
            updateWeather(WEATHER_PROXY);
        })
        .catch(error => alert(error));
}

function updateWeather(weatherData) {
    ELEMENTS.ELEMENTS_WEATHER_CITY.textContent = capitalize(weatherData.cityName);
    ELEMENTS.ELEMENTS_WEATHER_DESCRIPTION.textContent = weatherData.description;
    ELEMENTS.ELEMENTS_WEATHER_TEMPERATURE.textContent = weatherData.temperature;

    ELEMENTS.ELEMENTS_LOADING_TEXT.style.display = 'none';
    ELEMENTS.ELEMENTS_WEATHER_BOX.style.display = 'block';
}

const capitalize = (s) => {
    if (typeof s !== 'string') {
        return '';
    }
    return s.charAt(0).toUpperCase() + s.slice(1)
}

const validateString = (inputTxt) => {
    const REG = '[^A-Za-z]+';
    const re1 = new RegExp(REG);
    return re1.test(inputTxt);
}