export class WeatherData {
    constructor(cityName, description, metric) {
        this.cityName = cityName;
        this.description = description;
        this.temperature = '';
        this.metric = metric;
    }
}

export  const WEATHER_PROXY_HANDLER = {
    get: function (target, property) {
        return Reflect.get(target, property);
    },
    set: function (target, property, value) {
        let newValue =  target.metric == 'celsius'
            ? ((value - 32)/ 1.8).toFixed(2) + ' °C'
            : value + ' °F';
        //    : (value * 1.8 + 32).toFixed(2) + ' °F';
       //  const  newValue = ((value - 32)/ 1.8).toFixed(2) + ' °C';
        return Reflect.set(target, property, newValue);
    }
};