export const ELEMENT_SEARCH_BUTTON = document.querySelector('button');
export const ELEMENT_SEARCHED_CITY = document.querySelector('#city');

export const ELEMENTS_LOADING_TEXT = document.querySelector('#load');
export const ELEMENTS_WEATHER_BOX = document.querySelector('#weather');

export const ELEMENTS_WEATHER_CITY = ELEMENTS_WEATHER_BOX.firstElementChild;
export const ELEMENTS_WEATHER_DESCRIPTION = document.querySelector('#weatherDescription');
export const ELEMENTS_WEATHER_TEMPERATURE = ELEMENTS_WEATHER_BOX.lastElementChild;

export const ELEMENTS_RADIO_METRIC = document.querySelector('input[name="metric"]:checked');
