export class Validator {
  static validateString = (inputTxt) => {
        const REG = '[^A-Za-z]+';
        const re1 = new RegExp(REG);
        return re1.test(inputTxt);
    }
}